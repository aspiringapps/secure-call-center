﻿
using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Collections.Generic;

namespace MJPGApp
{
   [Activity(Label = "Phone", MainLauncher = false)]
    public class Phone:Activity
    {
       
        private List<KeyValuePair<string, string>> planets;


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "Main" layout resource
            SetContentView(Resource.Layout.SecondPage);

            //Button button = FindViewById<Button>(Resource.Id.myButton);
            EditText et1 = FindViewById<EditText>(Resource.Id.edit);
            String numbe = ((EditText)et1).ToString();

            Spinner spinner = FindViewById<Spinner>(Resource.Id.spinner);
            planets = new List<KeyValuePair<string, string>>
     {
                new KeyValuePair<string, string>("Abkhazia","7"),
                  new KeyValuePair<string, string>("Afghanistan" , "93"),
                  new KeyValuePair<string, string>("Aland Islanda","358"),
                  new KeyValuePair<string, string>("Albania","355"),
                  new KeyValuePair<string, string>("Algeria","213"),
                  new KeyValuePair<string, string>("American Samoa","1"),
                  new KeyValuePair<string, string>("Andorra","376"),
                  new KeyValuePair<string, string>("Angola","244"),
                  new KeyValuePair<string, string>("Anguilla","1"),
                  new KeyValuePair<string, string>("Antigua and Batbuda","1"),
                  new KeyValuePair<string, string>("Argentina","54"),
                  new KeyValuePair<string, string>("Armenia","374"),
                  new KeyValuePair<string, string>("Aruba","297"),
                  new KeyValuePair<string, string>("Ascension Island","247"),
                  new KeyValuePair<string, string>("Australia","61"),
                  new KeyValuePair<string, string>("Australian Antarctic Territory","672"),
                  new KeyValuePair<string, string>("Austria","43"),
                  new KeyValuePair<string, string>("Azerbaijan","994"),
                  new KeyValuePair<string, string>("Bahmas","1"),
                  new KeyValuePair<string, string>("Bahrain","973"),
                   new KeyValuePair<string, string>("Bangladesh", "880"),
                  new KeyValuePair<string, string>("Barbados","1"),
                  new KeyValuePair<string, string>("Belarus","375"),
                  new KeyValuePair<string, string>("Belgium","32"),
                  new KeyValuePair<string, string>("Belize","501"),
                  new KeyValuePair<string, string>("Benin","229"),
                  new KeyValuePair<string, string>("Bhutan","975"),
                  new KeyValuePair<string, string>("Botswana", "267"),
                  new KeyValuePair<string, string>("Brazil", "55"),
                  new KeyValuePair<string, string>("British Indian Ocean Territory", "246"),
                  new KeyValuePair<string, string>("British Virgin Islands", "1"),
                   new KeyValuePair<string, string>("Brunel","673"),
                   new KeyValuePair<string, string>("Bulgaria","359"),
                   new KeyValuePair<string, string>("Burkina Faso","226"),
                   new KeyValuePair<string, string>("Burma","95"),
                   new KeyValuePair<string, string>("Cambodia","885"),
                   new KeyValuePair<string, string>("Cameroon","237"),
                    new KeyValuePair<string, string>("Canada", "1"),
                   new KeyValuePair<string, string>("Canary islands","34"),
                   new KeyValuePair<string, string>("Cape Verde","238"),
                   new KeyValuePair<string, string>("Chile","56"),
                   new KeyValuePair<string, string>("France","33"),
                   new KeyValuePair<string, string>("Germany","49"),
                   new KeyValuePair<string, string>("GreenLand","229"),
                   new KeyValuePair<string, string>("India","91"),
                   new KeyValuePair<string, string>("Iran", "98"),
                   new KeyValuePair<string, string>("Israel","972"),
                   new KeyValuePair<string, string>("Malaysia","60"),
                   new KeyValuePair<string, string>("Maldives","960"),
                   new KeyValuePair<string, string>("New Zealand","64"),
                   new KeyValuePair<string, string>("Pakistan","92"),
};



            List<string> planetNames = new List<string>();
            foreach (var item in planets)
                planetNames.Add(item.Key);


            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);
            var adapter = new ArrayAdapter<string>(this,
    Android.Resource.Layout.SimpleSpinnerItem, planetNames);
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;


        }

        private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {

            Spinner spinner = (Spinner)sender;
            var numbe = FindViewById<EditText>(Resource.Id.edit).Text;
            // String number = editText.ToString();
            string toast = string.Format("The mean temperature for planet {0} is {1}",
                spinner.GetItemAtPosition(e.Position), planets[e.Position].Value);
            var uri = Android.Net.Uri.Parse("tel:" + planets[e.Position].Value + numbe);
            var intent = new Intent(Intent.ActionDial, uri);
            StartActivity(intent);
            // Toast.MakeText(this, toast, ToastLength.Long).Show();
        }

    }

}


